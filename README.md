# api-logging-dashboard

Implement log api endpoint with Golang that can push to keep in InfluxDB and visualize in Grafana dashboard.

## Getting started

Run service with
```
docker-compose up -d
```

After service is run successfully then you need **Map InfluxDB v2 buckets to InfluxDB v1 databases** with this rest api
```
curl --request POST http://localhost:8086/api/v2/dbrps \
  --header "Authorization: Token {YourAuthToken}" \
  --header 'Content-type: application/json' \
  --data '{
        "bucketID": "00oxo0oXx000x0Xo",
        "database": "example-db",
        "default": true,
        "orgID": "00oxo0oXx000x0Xo",
        "retention_policy": "example-rp"
      }'
```
