package config

import (
	"os"

	"github.com/joho/godotenv"
)

type APIConfig struct {
	APIKey string
}

type InfluxDBConfig struct {
	Token         string
	URL           string
	Org           string
	DefaultBucket string
}

// Config: config structure
type Config struct {
	APIConfig      APIConfig
	InfluxDBConfig InfluxDBConfig
}

func InitConfig() *Config {
	godotenv.Load()
	return &Config{
		APIConfig: APIConfig{
			APIKey: os.Getenv("API_KEY"),
		},
		InfluxDBConfig: InfluxDBConfig{
			Token:         os.Getenv("INFLUXDB_ADMIN_TOKEN"),
			URL:           "http://influxdb:8086",
			Org:           os.Getenv("INFLUXDB_ORG"),
			DefaultBucket: os.Getenv("INFLUXDB_BUCKET"),
		},
	}
}
