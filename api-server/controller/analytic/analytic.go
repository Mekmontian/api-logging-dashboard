package analytic

import (
	analyticService "api-logging-dashboard/service/analytic"
	"net/http"

	"github.com/gin-gonic/gin"
)

type AnalyticController struct {
	service analyticService.Interface
}

func NewAnalyticController(service analyticService.Interface) *AnalyticController {
	return &AnalyticController{
		service: service,
	}
}

func (impl *AnalyticController) ProduceMeasurement(c *gin.Context) {
	requestBody := produceMeasurementRequest{}
	if err := c.ShouldBindJSON(&requestBody); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if err := impl.service.ProduceMeasurement(
		requestBody.Measurement,
		requestBody.TagSet,
		requestBody.FieldSet,
	); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(200, gin.H{
		"status": "posted",
	})
}
