package analytic

import "github.com/gin-gonic/gin"

type Interface interface {
	ProduceMeasurement(c *gin.Context)
}
