package analytic

type produceMeasurementRequest struct {
	Measurement string                 `json:"measurement" binding:"required"`
	TagSet      map[string]string      `json:"tag"`
	FieldSet    map[string]interface{} `json:"field"`
}
