package influxdb

import (
	"context"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/influxdata/influxdb-client-go/v2/api/write"
)

type InfluxDBDatasource struct {
	client        influxdb2.Client
	org           string
	defaultBucket string
}

type InfluxDBConfig struct {
	Token         string
	URL           string
	Org           string
	DefaultBucket string
}

func NewInfluxDBDatasource(config InfluxDBConfig) (*InfluxDBDatasource, error) {
	client := influxdb2.NewClient(config.URL, config.Token)
	// validate client connection health
	_, err := client.Health(context.Background())
	return &InfluxDBDatasource{
		client:        client,
		org:           config.Org,
		defaultBucket: config.DefaultBucket,
	}, err
}

func (impl *InfluxDBDatasource) GetRawClient() influxdb2.Client {
	return impl.client
}

func (impl *InfluxDBDatasource) GetOrgName() string {
	return impl.org
}

func (impl *InfluxDBDatasource) GetDefaultBucketName() string {
	return impl.defaultBucket
}

func (impl *InfluxDBDatasource) CreatePointWithMeasurement(name string) *write.Point {
	return influxdb2.NewPointWithMeasurement(name)
}
