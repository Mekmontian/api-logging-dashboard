package influxdb

import (
	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/influxdata/influxdb-client-go/v2/api/write"
)

type Interface interface {
	GetRawClient() influxdb2.Client
	GetOrgName() string
	GetDefaultBucketName() string
	CreatePointWithMeasurement(name string) *write.Point
}
