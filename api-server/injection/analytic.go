package injection

import (
	analyticController "api-logging-dashboard/controller/analytic"
	"api-logging-dashboard/db/influxdb"
	analyticRepository "api-logging-dashboard/repository/analytic"
	analyticRoutes "api-logging-dashboard/routes/analytic"
	analyticService "api-logging-dashboard/service/analytic"
)

type AnalyticModuleConfig struct {
	InfluxDB influxdb.Interface
}

func NewAnalyticModule(config AnalyticModuleConfig) analyticRoutes.Interface {
	repository := provideAnalyticRepository(config.InfluxDB)
	service := provideAnalyticService(repository)
	controller := provideAnalyticController(service)
	return analyticRoutes.NewAnalyticRoutes(controller)
}

func provideAnalyticRepository(influxDB influxdb.Interface) analyticRepository.Interface {
	return analyticRepository.NewAnalyticRepository(influxDB)
}

func provideAnalyticService(repository analyticRepository.Interface) analyticService.Interface {
	return analyticService.NewAnalyticService(repository)
}

func provideAnalyticController(service analyticService.Interface) analyticController.Interface {
	return analyticController.NewAnalyticController(service)
}
