package main

import (
	"api-logging-dashboard/config"
	"api-logging-dashboard/db/influxdb"
	"api-logging-dashboard/injection"
	"api-logging-dashboard/middleware"
	"log"

	"github.com/gin-gonic/gin"
)

func main() {
	cfg := config.InitConfig()
	db, err := influxdb.NewInfluxDBDatasource(influxdb.InfluxDBConfig{
		Token:         cfg.InfluxDBConfig.Token,
		URL:           cfg.InfluxDBConfig.URL,
		Org:           cfg.InfluxDBConfig.Org,
		DefaultBucket: cfg.InfluxDBConfig.DefaultBucket,
	})
	if err != nil {
		log.Panicln("Can't not connect to db")
	}
	router := gin.Default()
	router.GET("/heartbeat", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"status": "ok",
		})
	})
	v1 := router.Group("/v1")
	v1.Use(middleware.APIKeyMiddleware(cfg.APIConfig.APIKey))
	injection.NewAnalyticModule(injection.AnalyticModuleConfig{
		InfluxDB: db,
	}).InitialRoutes(v1)
	router.Run(":8888")
}
