package middleware

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

func APIKeyMiddleware(apiKey string) gin.HandlerFunc {
	if apiKey == "" {
		log.Fatal("Please set API_KEY environment variable")
	}

	return func(c *gin.Context) {
		key := c.Request.Header.Get("x-api-key")

		if key == "" {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "API key required"})
			return
		}

		if key != apiKey {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "Invalid API token"})
			return
		}

		c.Next()
	}
}
