package analytic

import (
	"api-logging-dashboard/db/influxdb"
	"context"
	"time"
)

type AnalyticRepository struct {
	influxDB influxdb.Interface
}

func NewAnalyticRepository(influxDB influxdb.Interface) *AnalyticRepository {
	return &AnalyticRepository{
		influxDB: influxDB,
	}
}

func (impl *AnalyticRepository) ProduceMeasurement(name string, tagSet map[string]string, fieldSet map[string]interface{}, t time.Time) error {
	writeAPI := impl.influxDB.GetRawClient().WriteAPIBlocking(impl.influxDB.GetOrgName(), impl.influxDB.GetDefaultBucketName())
	point := impl.influxDB.CreatePointWithMeasurement(name)
	for key, value := range tagSet {
		point.AddTag(key, value)
	}
	for key, value := range fieldSet {
		point.AddField(key, value)
	}
	point.SetTime(t)
	return writeAPI.WritePoint(context.Background(), point)
}
