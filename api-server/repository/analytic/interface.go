package analytic

import "time"

type Interface interface {
	ProduceMeasurement(name string, tagSet map[string]string, fieldSet map[string]interface{}, t time.Time) error
}
