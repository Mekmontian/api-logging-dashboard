package analytic

import (
	analyticController "api-logging-dashboard/controller/analytic"

	"github.com/gin-gonic/gin"
)

type AnalyticRoutes struct {
	controller analyticController.Interface
}

func NewAnalyticRoutes(controller analyticController.Interface) *AnalyticRoutes {
	return &AnalyticRoutes{
		controller: controller,
	}
}

func (impl *AnalyticRoutes) InitialRoutes(router *gin.RouterGroup) {
	group := router.Group("/analytic")
	group.POST("/measure", impl.controller.ProduceMeasurement)
}
