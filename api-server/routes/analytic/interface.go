package analytic

import "github.com/gin-gonic/gin"

type Interface interface {
	InitialRoutes(router *gin.RouterGroup)
}
