package analytic

import (
	analyticRepository "api-logging-dashboard/repository/analytic"
	"time"
)

type AnalyticService struct {
	repository analyticRepository.Interface
}

func NewAnalyticService(repository analyticRepository.Interface) *AnalyticService {
	return &AnalyticService{
		repository: repository,
	}
}

func (impl *AnalyticService) ProduceMeasurement(name string, tagSet map[string]string, fieldSet map[string]interface{}) error {
	return impl.repository.ProduceMeasurement(name, tagSet, fieldSet, time.Now())
}
