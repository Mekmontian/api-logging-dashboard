package analytic

type Interface interface {
	ProduceMeasurement(name string, tagSet map[string]string, fieldSet map[string]interface{}) error
}
